picturae-dev
============

**A Symfony and Docker project that handles all your development needs.**

It includes a private wordpress blog where you can store your development notes & activities, which you can easily transfer to the global blog if needed because its based on wordpress too.

> It would be wise to setup a global development blog aswell where the bigger software changes and projects are announced with links to the repositories and wiki's. This way everyone can easily track and anticipate the companies development activities.

The project makes use of `docker-compose` for running the images.

**Docker images:**

* webserver
* php-fpm
* php-fpm-phalcon
* mysql
* mongo
* postgis (postgres)
* solr
* memcached
* jena (sparql)


## How to install

Clone this repository in your `Projects` folder.

    git clone git@gitlab.com:rickoverman/picturae-dev.git

Select project folder and Install composer packages.

    cd picturae-dev && composer install


## Getting Started

There are a few handy console commands that you can use to setup your development environment.

Start the images

    bin/console up

Stop all the docker images 

    bin/console stop









