<?php
if (!function_exists('isPictura')) {

    function isPictura()
    {
        return true;
    }
}

// DIRTY SESSION HACK
if (isset($_POST['onthoudmij'])) {
    ini_set('session.cookie_lifetime', (int) $_POST['onthoudmij']);
} else if (isset($_COOKIE['mmm_cookie_lifetime'])) {
    ini_set('session.cookie_lifetime', (int) $_COOKIE['mmm_cookie_lifetime']);
}

// Define path to application directory
defined('APPROOT') || define('APPROOT', realpath(dirname(__FILE__) . '/../'));

// Define path to application directory
defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// JS compress settings
defined('YUI_COMPRESSOR') || define('YUI_COMPRESSOR', (getenv('YUI_COMPRESSOR') ? getenv('YUI_COMPRESSOR') : true));

// Define Double Quoute so we can use it in INI files:
if (!defined('DQUOTE')) {
    define('DQUOTE', '"');
}

/*
// Typically, you will also want to add your library/ directory
// to the include_path, particularly if it contains your ZF install
$memorixPaths = array(
    dirname(dirname(__FILE__)) . '/library/memorix/',
    dirname(dirname(__FILE__)) . '/library/zend/',
    dirname(dirname(__FILE__)) . '/library/solr-client/',
    dirname(dirname(__FILE__)) . '/library/picl/',
    dirname(dirname(__FILE__)) . '/library/phpexcel/',
);

$phpPaths = explode(PATH_SEPARATOR, get_include_path());

$paths = array_merge($memorixPaths, $phpPaths);

set_include_path(implode(PATH_SEPARATOR, $paths));
*/
require_once '../vendor/autoload.php';

//cache in very early state:
require_once 'Zend/Cache.php';
$frontendOptions = array('automatic_serialization' => true);
$backendOptions = array('cache_dir' => dirname(dirname(__FILE__)) . '/cache/');

 


$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);

// Next, set the cache to be used with all table objects
require_once 'Zend/Db/Table/Abstract.php';
Zend_Db_Table_Abstract::setDefaultMetadataCache($cache);

// Next, set the cache to be used with Translate
require_once 'Zend/Translate.php';
Zend_Translate::setCache($cache);

require_once 'Memorix.php';

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(APPLICATION_ENV, APPLICATION_PATH . '/../config/application.ini');

$application->bootstrap()->run();

