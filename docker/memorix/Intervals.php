<?php
require_once APPLICATION_PATH . '/gebruikersbeheer/models/User.php';
require_once APPLICATION_PATH . '/gebruikersbeheer/models/Pref.php';
require_once APPLICATION_PATH . '/gebruikersbeheer/models/UserPref.php';
require_once APPLICATION_PATH . '/portaal/models/Mantis.php';

/**
 * Checks if there is any change in the user tickets and display a message to the user
 */
class Memorix_Controller_Plugin_Intervals extends Zend_Controller_Plugin_Abstract
{

    /**
     * @param Zend_Controller_Request_Abstract $request
     */
    public function routeStartup(Zend_Controller_Request_Abstract $request)
    {
        if ($request->isXmlHttpRequest()) return;
    }


    //Will be processed and showed in order.
    private static $intervals = array (
        'interval_alerts' => array (
            'period' => 2,
            'callback' => 'processAlerts'
        ),
        'interval_tickets' => array (
            'period' => 240,
            'callback' => 'processTickets'
        )
    );

    /**
     *
     * @var array
     */
    private $identityInfo;

    /**
     *
     * @var Zend_Auth_Storage_Session
     */
    private $sessionStorage;

    /**
     *
     * @var Algemeen_Model_User
     */
    private $user;
    /**
     *
     * @var array
     */
    private $queue;

    /**
     *
     */
    private $timestamps;

    /**
     * @return Memorix_Controller_Plugin_Intervals
     */
    private function setIdentityInfo()
    {
        $this->identityInfo = Zend_Auth::getInstance()->getIdentity();

        return $this;
    }

    /**
     * @return array
     */
    private function getIdentityInfo()
    {
        return $this->identityInfo;
    }

    /**
     * @return Memorix_Controller_Plugin_Intervals
     */
    private function setSessionStorage()
    {
        $this->sessionStorage = Zend_Auth::getInstance()->getStorage();

        return $this;
    }

    /**
     * @return Zend_Auth_Storage_Session
     */
    private function getSessionStorage()
    {
        return $this->sessionStorage;
    }

    /**
     * @return Memorix_Controller_Plugin_Intervals
     */
    private function writeSession()
    {
        try {
            $this->getSessionStorage()->write($this->getIdentityInfo());
        } catch (Exception $exc) {
            $log = Memorix::errorLogger();
            $log->log('Cannot write version to session on line: "' . __LINE__ . ' file: "' . __FILE__ . '" ' . $exc->getMessage(), Zend_Log::WARN);
        }

        return $this;
    }

    /**
     * @return Memorix_Controller_Plugin_Intervals
     */
    private function setUser()
    {
        $users = new Algemeen_Model_UsersMapper();
        $this->user = $users->getByUuid($this->identityInfo['uuid']);
        $users->save($this->user);
        $this->user->refresh();
        $this->identityInfo['modified_time'] = $this->user->getModified_time();

        return $this;
    }

    /**
     * @return Algemeen_Model_User
     */
    private function getUser()
    {
        return $this->user;
    }

    /**
     *
     * @return array
     */
    public function getQueue()
    {
        return is_array($this->queue) ? $this->queue : array();
    }

    /**
     *
     * @return array
     */
    public function getTimestamps()
    {
        return is_array($this->timestamps) ? $this->timestamps : array();
    }

    /**
     * @return integer
     */
    private function getLoginTimestamp()
    {
        $identity = $this->getIdentityInfo();

        return strtotime($identity['modified_time']);
    }

    public function routeShutdown(Zend_Controller_Request_Abstract $request)
    {
        try {
            if (!$request->isXmlHttpRequest() && Zend_Auth::getInstance()->hasIdentity()) {
                $this->setIdentityInfo()
                    ->setSessionStorage();
                $lastLogin = $this->getLoginTimestamp();
                $this->setUser();

                foreach (static::$intervals as $intervalKey => $intervalConfig) {
                    if (isset($this->identityInfo[$intervalKey])
                        && ($this->identityInfo[$intervalKey] + ($intervalConfig['period'] * 60)) > time()
                    ) {
                        break;
                    }

                    if (!isset($this->identityInfo[$intervalKey])) {
                        $this->identityInfo[$intervalKey] = $lastLogin;
                    }

                    $updatedSince = $this->identityInfo[$intervalKey];

                  //  $output = $this->$intervalConfig['callback']($updatedSince);

                    // if (!is_null($output)) {
                    //     $this->queue($output, $intervalKey);
                    // }

                    //set time of last occurrence
                    $this->identityInfo[$intervalKey] = time();
                }
                $this->writeSession()
                    ->processKeepAlive()
                    ->processQueue();
            }
        } catch (\Exception $e) {
            //we don't want to break the application just because one job fails(most likely Mantis is down or timed out)
            trigger_error($e->getMessage());
        }
    }

    /**
     * Get updated issues from mantis.
     * @param  integer $lastRun
     * @return mixed
     */
    private function processTickets($lastRun)
    {
        try {
            $mantis = new Portaal_Model_Mantis();
        } catch (Exception $exc) {
            return;
        }

        $filter = array(
            'reporter_id' => $mantis->p_user_id,
        );

        $issues = $mantis->getIssues($filter);
        $changedIssues = array();
        foreach ($issues as $issue) {
            $lastUpdated = strtotime($issue['last_updated']);
            if ($lastRun < $lastUpdated) {
                $status = $issue['status'];
                $changedIssues[$status][] = $issue;
            }
        }

        if (!empty($changedIssues)) {
            $message = "<h1>" . Memorix::translate('Tickets wijzigingen') . "</h1>";
            $message .= '<p>' . Memorix::translate('Sinds uw laatste bezoek zijn er wijzigingen geweest in u tickets.').'"</p>';

            foreach ($changedIssues as $status => $statusIssues) {
                $message .= '<h3>'.ucfirst(Memorix::translate($status)).'</h3><ul>';
                foreach ($statusIssues as $issue) {

                    $href = null;
                    $url = '/%s/%s/ticket/'.$issue['id'];
                    switch ($issue['category']) {
                        case 'helpdesk':
                            $href = sprintf($url, Memorix::translate('helpdesk'), Memorix::translate('detail'));
                            break;
                        case 'idee':
                            $href = sprintf($url, Memorix::translate('idee'), Memorix::translate('detail'));
                            break;
                        case 'klachten':
                            $href = sprintf($url, Memorix::translate('klachten'), Memorix::translate('detail'));
                            break;
                        case 'projectissues':
                            $href = sprintf($url, Memorix::translate('projectissues'), Memorix::translate('detail'));
                            break;
                    }

                    if (!$href) {
                        continue;
                    }

                    $timestamp = strtotime($issue['last_updated']);
                    $modifyDate = date('Y-m-d H:i', $timestamp);
                    $modified = Memorix::translate('aangepast op');

                    $value = htmlspecialchars($issue['summary']).' ('.$modified.' '.$modifyDate.')';
                    $link = '<a href="'.$href.'">'.$value.'</a>';
                    $message .= '<li>'.$link.'</li>';
                }
            }

            return $message;
        }

        return null;
    }

    /**
     * Get announcements from Mantis.
     * @param  integer $lastRun
     * @return mixed
     */
    private function processAlerts($lastRun)
    {
        try {
            $mantis = new Portaal_Model_Mantis();
        } catch (Exception $exc) {
            return;
        }

        $announcements = $mantis->getAnnounce();

        //$lastRun = strtotime("-1 day");

        $alerts = '';
        if (!empty($announcements)) {
            $announcements = array_reverse(
                array_filter($announcements, function ($announcement) use ($lastRun) {
                        //Memorix::LogData('Testing '.$announcement->headline . date('Y-m-d H:i:s', $lastRun ) . ' vs ' . $announcement->last_modified ,'alert');
                        return $lastRun < strtotime($announcement->last_modified);
                })
            );
            $html = "<h1>" . Memorix::translate('Memorix message') . "</h1>";
            $alerts = array_reduce($announcements, function ($result, $announcement) {
                $result .= '<h3>' . $announcement->headline .  '</h3>';
                $result .= '<p>' . $announcement->body . '</p>';

                return $result;
            }, $alerts);
        }

        return empty($alerts) ? null : $html . $alerts;
    }

    /**
     * Write the keep alive refresh...
     */
    private function processKeepAlive()
    {
        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        $js = <<<JS
            window.addEvent('domready', function() {
                var keepAlive = new Request({
                    method: 'get', url: '/gebruikersbeheer/keepalive'
                });
                keepAlive.send.periodical(1000 * mmmConfig.session.timeout, keepAlive);
            });
JS;
        $view->headScript()->appendScript($js);

        return $this;
    }

    /**
     * Recursive squeezebox windows with all messages.
     * There is some problem with mantis html outputs....
     * @
     */
    private function processQueue()
    {
        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');

        $data = $this->getQueue();

        $messages = json_encode(
            array_map(
                function ($el) {
                    return strtr($el, array(
                        "\r\n" => '',
                        '"' => '\"'
                    )
                    );
                },
                array_values($data)
            )
        );

        $messages = strtr($messages, array('\'' => '\\\''));
        $messageTypes = json_encode(array_keys($data));
        $timestamps = json_encode(array_values($this->getTimestamps()));
        $stateUrl = $view->getHelper('url')->url(
            array(
                'module' => 'algemeen',
                'controller' => 'alerts',
                'action' => 'save'
            ),
            null,
            false
        );

        $js = <<<JS
            window.addEvent('domready', function () {
                var messages = JSON.decode('{$messages}');
                var messageTypes = JSON.decode('{$messageTypes}');
                var stateUrl = '{$stateUrl}';
                var timestamps = JSON.decode('{$timestamps}');

                var alerts = function () {
                    var element = null;

                    var callback = function (e) {
                        if (e && e.preventDefault) {
                            e.preventDefault();
                            var link = this.get('href');
                        }
                        if (messageTypes.length > 0) {
                            alerts();
                        }
                        new Request.JSON({
                            'url': stateUrl,
                            'onComplete': function() {if(link) document.location=link}
                        }).get({'type' : messageTypes.shift(), 'ts' : timestamps.shift()});
                    };

                    if (messages.length > 0) {
                        element = new Element('div', { 'html': messages.shift()})
                            .addEvent('click:relay(a)', callback);
                        SqueezeBox.open(
                            element,
                            {
                                handler: 'adopt',
                                size: {x: 450, y: 350},
                                onClose: callback
                            }
                        );
                    }
                };
                alerts();
            });
JS;
        $view->headScript()->appendScript($js);

        return $this;
    }

    private function queue($output, $index)
    {
        if (!is_array($this->queue)) {
            $this->queue = array();
        }
        $this->queue[$index] = $output;
        $this->timestamps[$index] = time();

        return $this;
    }
}
