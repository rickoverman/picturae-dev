
#maybe should be
# /var/lib/postgresql


CREATE TABLESPACE share
    -- [ OWNER user_name ]
    LOCATION '/var/lib/postgresql/data'
    -- [ WITH ( tablespace_option = value [, ... ] ) ]
;

CREATE TABLESPACE _temp
    -- [ OWNER user_name ]
    LOCATION '/var/lib/postgresql/data'
    -- [ WITH ( tablespace_option = value [, ... ] ) ]
;

CREATE TABLESPACE rmt
    -- [ OWNER user_name ]
    LOCATION '/var/lib/postgresql/data'
    -- [ WITH ( tablespace_option = value [, ... ] ) ]
;
