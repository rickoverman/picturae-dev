
-- indexer scheme for the temporary templates
CREATE SCHEMA indexer
    AUTHORIZATION mmm_owner;

CREATE SCHEMA _temp;
    AUTHORIZATION mmm_owner;


-- CREATE TABLE _temp.index
-- (
--     uuid uuid,
--     created timestamp without time zone,
--     modified timestamp without time zone,
--     tenant character varying(64),
--     owner uuid,
--     sessionid character(32),
--     route character varying(255),
--     state character varying(10),
--     table_identifier character varying(128),
--     CONSTRAINT index_pkey PRIMARY KEY (uuid)
--         USING INDEX TABLESPACE _temp,
--     CONSTRAINT index_owner_key UNIQUE (owner, route, sessionid)
--         USING INDEX TABLESPACE _temp
-- )