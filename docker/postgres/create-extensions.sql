CREATE EXTENSION hstore
    SCHEMA public
    VERSION "1.4";

CREATE EXTENSION ltree
    SCHEMA public
    VERSION "1.1";

-- CREATE EXTENSION postgis
--     SCHEMA public
--     VERSION "2.3.0";

-- CREATE EXTENSION postgis_topology
--     SCHEMA topology
--     VERSION "2.3.0";

CREATE EXTENSION tablefunc
    SCHEMA public
    VERSION "1.0";

CREATE EXTENSION "uuid-ossp"
    SCHEMA public
    VERSION "1.1";