CREATE TABLE "_temp"."index" (
	uuid uuid,
	sessionid text,
	route text,
	table_identifier text,
	owner text,
	CONSTRAINT "_temp_index_pkey" PRIMARY KEY (uuid)
	
)
WITH (
	OIDS=FALSE
) ;
