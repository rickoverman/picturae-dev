export PGPASSWORD=postgres
docker exec -it postgis pwd
# preperations needed before schema restore
docker exec -it postgis psql -h 127.0.0.1 -p 5432 -U postgres -d memorix_conversion -a -w -f postgres/create-tablespaces.sql
docker exec -it postgis psql -h 127.0.0.1 -p 5432 -U postgres -d memorix_conversion -a -w -f postgres/create-extensions.sql
docker exec -it postgis psql -h 127.0.0.1 -p 5432 -U postgres -d memorix_conversion -a -w -f postgres/create-users.sql
docker exec -it postgis psql -h 127.0.0.1 -p 5432 -U postgres -d memorix_conversion -a -w -f postgres/create-schemas.sql

# fixes for handling current database configuration mistakes.

# ftp_login fix when restoring the public scheme
docker exec -it postgis psql -h 127.0.0.1 -p 5432 -U postgres -d memorix_conversion -a -w -f postgres/fixes.sql

# schema restore
# docker exec -it postgis pg_restore -h 127.0.0.1 -p 5432 -U postgres -d memorix_conversion postgres/drop-schema.psql
docker exec -it postgis pg_restore -h 127.0.0.1 -p 5432 -U postgres -d memorix_conversion postgres/pgdumps/public-current.psql
docker exec -it postgis pg_restore -h 127.0.0.1 -p 5432 -U postgres -d memorix_conversion postgres/pgdumps/share-current.psql
docker exec -it postgis pg_restore -h 127.0.0.1 -p 5432 -U postgres -d memorix_conversion postgres/pgdumps/rmt-current.psql   