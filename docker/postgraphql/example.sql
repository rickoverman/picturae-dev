-- { testById(uuid: '70936e8e-0cbe-11e7-ab2a-8f8be40e3669') { uuid name } }


CREATE SCHEMA graphql;

CREATE TABLE "graphql"."test" (
	uuid uuid,
	name text,
	username text,
	password text,
	email text,
	CONSTRAINT "public_test_pkey" PRIMARY KEY (uuid)
)
WITH (
	OIDS=FALSE
);


insert into graphql.test (uuid,"name", username,email)values
('70936e8e-0cbe-11e7-ab2a-8f8be40e3669', 'Rick Overman', 'rickoverman', 'r.overman@picturae.com');

insert into graphql.test (uuid,"name", username,email)values
('596aa032-0cbf-11e7-b36b-038607eddf4b', 'Administrator', 'admin', 'admin@picturae.com');

