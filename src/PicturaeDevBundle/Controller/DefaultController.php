<?php

namespace PicturaeDevBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('PicturaeDevBundle:Default:index.html.twig');
    }
}
